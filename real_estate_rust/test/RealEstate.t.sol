// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import {RealEstate} from "../src/RealEstate.sol";

contract RealEstateTest is Test {
    RealEstate public realEstate;
    address owner = makeAddr("owner");
    address tennant = makeAddr("tennant");
    
    uint installments = 5;
    uint installmentPrice = 1000;
    uint paymentInterval = 60 * 1;
    uint extensionFirst = 60 * 1;
    uint interestFirst = 1000;
    uint extensionSecond = 60 * 2;
    uint interestSecond = 2000;

    function setUp() public {
        realEstate = new RealEstate();
    }

    function setupCreateProperty () private returns(uint) {
        vm.startPrank(owner);
        uint id = realEstate.createProperty();
        vm.stopPrank();
        return id;
    }

    function setupCreateLease (uint _id) private {
        vm.startPrank(owner);
        realEstate.createLease(
            _id,
            installmentPrice,
            installments,
            paymentInterval,
            extensionFirst,
            extensionSecond,
            interestFirst,
            interestSecond
        );
        vm.stopPrank();
    }
    function setupStartLease () private {
        vm.startPrank(tennant);
        vm.deal(tennant, 99000);
        realEstate.startLease(0);
        vm.stopPrank();
    }

    function test_startLease() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();

        vm.startPrank(tennant);
        assertEq(realEstate.propertyHead(), 1);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        skip(paymentInterval + 20);
        realEstate.payLease{value: 2000}(0);
    }

    function test_lastSecondLease() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();

        vm.startPrank(tennant);
        assertEq(realEstate.propertyHead(), 1);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        skip(paymentInterval + (extensionSecond -1));
        realEstate.payLease{value: 3000}(0);
        skip(paymentInterval - 1);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        realEstate.createLease(
            id,
            installmentPrice,
            installments,
            paymentInterval,
            extensionFirst,
            extensionSecond,
            interestFirst,
            interestSecond
        );
    }

    function testFail_delayedPaymentFails() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();

        vm.startPrank(tennant);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        skip(paymentInterval + 20);
        realEstate.payLease{value: 1000}(0);
    }

    function test_withdrawal() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();

        vm.startPrank(tennant);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);
        realEstate.payLease{value: 1000}(0);

        assertEq(realEstate.getPropertyOwner(0), tennant);
        assertNotEq(realEstate.getPropertyOwner(0), owner);
    }


    function  testFail_payLease() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();
        
        skip((paymentInterval * 3) + 10);
        vm.startPrank(tennant);
        vm.deal(tennant, 9999999999);
        realEstate.payLease(0);
    }

    function testDeactivate() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();
        skip(paymentInterval + extensionSecond + 1);
        vm.startPrank(owner);
        realEstate.deactivateProperty(id);
    }

    function testFail_Deactivate() public {
        uint id = setupCreateProperty();
        setupCreateLease(id);
        setupStartLease();
        vm.startPrank(owner);
        realEstate.deactivateProperty(id);
    }
}
