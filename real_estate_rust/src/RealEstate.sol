// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

struct Property {
    address owner;
    Lease lease;
    bool isActive;
}

struct Lease {
    bool isActive;
    bool isStarted;
    address tennant;
    uint installmentPrice;
    uint timestampLastPayment;
    uint installmentsRemaining;
    uint paymentInterval;
    uint extensionFirst;
    uint interestFirst;
    uint extensionSecond;
    uint interestSecond;
    uint balance;
}

contract RealEstate {
    uint public propertyHead = 0;

    mapping(uint => Property) public properties;

    modifier onlyValidProperty(uint _id) {
        require(_id < propertyHead, "Property doesn't exist");
        require(properties[_id].isActive, "Property isn't active");
        _;
    }

    modifier onlyPropertyOwner(uint _id) {
        require(properties[_id].owner == msg.sender, "Only the owner of the property can perform this operation");
        _;
    }

    modifier onlyActiveLease(uint _id) {
        require(properties[_id].lease.isActive, "Lease must be active");
        _;
    }

    modifier onlyInactiveLease(uint _id) {
        require(!properties[_id].lease.isActive, "Lease must be inactive");
        _;
    }

    modifier onlyStartedLease(uint _id) {
        require(properties[_id].lease.isStarted, "Lease must be started");
        _;
    }

    modifier onlyNonstartedLease(uint _id) {
        require(!properties[_id].lease.isStarted, "Lease is already started");
        _;
    }

    modifier onlyTennant(uint _id) {
        require(properties[_id].lease.tennant == msg.sender, "Oly the tennant of this property can perform this operation");
        _;
    }

    constructor() { }

    function emptyLease() private pure returns(Lease memory) {
        return Lease({
            isActive: false,
            isStarted: false,
            tennant: address(0),
            installmentPrice: 0,
            timestampLastPayment: 0,
            installmentsRemaining: 0,
            paymentInterval: 0,
            extensionFirst: 0,
            extensionSecond: 0,
            interestFirst: 0,
            interestSecond: 0,
            balance: 0
        });
    }

    function createProperty() public returns(uint) {

        properties[propertyHead] = Property({
            owner: msg.sender,
            lease: emptyLease(),
            isActive: true
        });

        propertyHead += 1;
        return propertyHead -1;  
    }

    function createLease
    (
        uint _id,
        uint _installmentPrice,
        uint _installments,
        uint _paymentInterval,
        uint _extensionFirst,
        uint _extensionSecond,
        uint _interestFirst,
        uint _interestSecond
    ) public
        onlyPropertyOwner(_id)
        onlyInactiveLease(_id)
        onlyNonstartedLease(_id)
        onlyValidProperty(_id)
    {
        require(_installmentPrice > 0, "Installment price must be > 0");
        require(_installments > 0, "Installments must be > 0");
        require(_paymentInterval > 0, "Payment interval must be > 0");
        require(_extensionFirst> 0, "First extension must be > 0");
        require(_extensionSecond > _extensionFirst, "Second extension must be longer than first extension");
        require(_interestFirst > 0, "First interest must be > 0");
        require(_interestSecond > 0, "Second interest must be > 0");

        properties[_id].lease.isActive = true;
        properties[_id].lease.timestampLastPayment = block.timestamp;
        properties[_id].lease.balance = 0;
        properties[_id].lease.installmentPrice = _installmentPrice;
        properties[_id].lease.installmentsRemaining = _installments;
        properties[_id].lease.paymentInterval = _paymentInterval;
        properties[_id].lease.extensionFirst = _extensionFirst;
        properties[_id].lease.extensionSecond = _extensionSecond;
        properties[_id].lease.interestFirst = _interestFirst;
        properties[_id].lease.interestSecond = _interestSecond;
    }

    function withdraw(uint _id) public 
        onlyPropertyOwner(_id)
        onlyActiveLease(_id)
        onlyStartedLease(_id)
        onlyValidProperty(_id)
    {
        uint balance = properties[_id].lease.balance;
        require(balance > 0, "Cannot withdraw from property that has an empty balance");
        payable(msg.sender).transfer(balance);
        properties[_id].lease.balance = 0;
    }

    function startLease(uint _id) public
        onlyActiveLease(_id)
        onlyNonstartedLease(_id)
        onlyValidProperty(_id) 
    {
        properties[_id].lease.isStarted = true;        
        properties[_id].lease.tennant = msg.sender;
        properties[_id].lease.timestampLastPayment = block.timestamp;
    }

    function calculateNextPayment(uint _id) public view 
        onlyValidProperty(_id) 
        onlyActiveLease(_id) 
        onlyStartedLease(_id)
        returns(uint) 
    {
        Lease memory lease = properties[_id].lease; 
        uint timeDelta = block.timestamp - lease.timestampLastPayment;

        if(timeDelta < lease.paymentInterval) {
            return lease.installmentPrice; 
        } else if (timeDelta < (lease.paymentInterval + lease.extensionFirst)) {
            return lease.installmentPrice + lease.interestFirst; 
        } else if (timeDelta < (lease.paymentInterval + lease.extensionSecond)) {
            return lease.installmentPrice + lease.interestSecond;
        }else{
            return 0;
        }
    }

    function payLease(uint _id) public payable
        onlyActiveLease(_id)
        onlyStartedLease(_id)
        onlyValidProperty(_id)
        onlyTennant(_id)
    {
        Lease memory lease = properties[_id].lease;
        require(lease.installmentsRemaining > 0, "Lease is payed off");
        uint payment = calculateNextPayment(_id);
        require(payment > 0, "The lease is no longer valid due to missed payments");
        require(!(msg.value < payment), "The provided payment is too low");
        require(!(msg.value > payment), "The provided payment is too high");

        properties[_id].lease.balance += msg.value;
        properties[_id].lease.installmentsRemaining -= 1;
        properties[_id].lease.timestampLastPayment = block.timestamp;

        if(properties[_id].lease.installmentsRemaining == 0) {
            if(lease.balance > 0) {
                payable(properties[_id].owner).transfer(lease.balance);
                properties[_id].lease.balance = 0;
            }
            properties[_id].lease = emptyLease();
            properties[_id].owner = lease.tennant;
        }
    }

    function deactivateProperty(uint _id) public
        onlyPropertyOwner(_id)
        onlyValidProperty(_id)
        onlyActiveLease(_id)
    {
        Lease memory lease = properties[_id].lease;
        require(lease.balance == 0, "Lease balance non 0. Please withdraw first");
        if(lease.isStarted) {
            require(calculateNextPayment(_id) == 0, "Lease must expire first");
        }
        properties[_id].isActive = false;
        properties[_id].lease = emptyLease();
    }

    function getPropertyOwner(uint _id) public view
        onlyValidProperty(_id)
        returns(address)
    {
        return properties[_id].owner;
    }
}
