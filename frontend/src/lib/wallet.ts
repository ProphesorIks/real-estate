import { writable } from "svelte/store";
import { contract, web3 } from "./contract";
import {Web3} from 'web3';
import abi from './json/abi.json'

export const account = writable<string>(undefined);

account.subscribe((acc) => {
    console.log("test", acc)
    if(acc !== undefined) {
        const w = new Web3(window["ethereum"])
        web3.set(w);
        contract.set(new w.eth.Contract(abi, "0x8acb2fd29497a5efb434c06c4f1a4fdb7b5fcd57"))
    }
})