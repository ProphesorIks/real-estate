import { writable } from "svelte/store";

// set a provider in the sepolia testnet using node rpc url
export const web3 = writable(undefined)
export const contract = writable(undefined)