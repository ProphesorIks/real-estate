import { writable } from "svelte/store"
import { v4 as uuidv4 } from 'uuid';

type Alert =  {
    message: string, 
    uuid: string,
    error?: boolean
}

export const alertStack = writable<Alert[]>([
]);
const isString = (value: unknown) => typeof value === 'string' || value instanceof String;

export const doAlert = (message: string, error = false) => {
    const m = isString(message) ? message : JSON.stringify(message)
    alertStack.update((s) => {
       return [...s, {
        message: m,
        uuid: uuidv4(),
        error
       }]
    })
}

export const dismiss = (id: string) => {
    alertStack.update((s) => {
        return s.filter(a => a.uuid !== id )
    })
}